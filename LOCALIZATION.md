## Localization

Localization is core to the use case for Butter Box.  Because this project
installs sources from all over, there are many different methods of
localization with varying language support.  The table below tracks the
availability of languages in Butter Box's various functions:

|Function                      | `en` | `es` | `zh` | `bo` | `pt` | `fr` | Notes |
|------------------------------|------|------|------|------|------|------|-------|
|Documentation: README.md and USER GUIDE.md - [Weblate](https://hosted.weblate.org/projects/guardianproject/butterbox-rpi/#repository)   |✅|✅|✅|✅|✅|✅|
|[butter-box-ui](https://gitlab.com/likebutter/butter-box-ui/) - [Weblate](https://hosted.weblate.org/projects/guardianproject/butter-box-ui/) |✅|✅|✅|✅|✅|✅| User-selectable.  Defaults to language Butter Box was provisioned for.
|[Butter App](https://gitlab.com/likebutter/butterapp) - [Weblate](https://hosted.weblate.org/projects/guardianproject/butterapp/)        |✅|✅|✅|✅|✅|✅| Automatically displayed based on Android language.
|[Apps within Butter App](https://likebutter.app/apps/)        |✅|🟡|🟡|🟡|🟡|🟡| App-specific support.  See each app for their localization status and pipeline.
|[RaspAP Admin Interface](https://raspap.com/) - [locales](https://github.com/RaspAP/raspap-webgui/tree/master/locale)        |✅|✅|✅|❌|✅|✅| Served based on browser's preferred language. See [docs](https://docs.raspap.com/translations/).
|[Keanu-Weblite chat](https://gitlab.com/keanuapp/keanuapp-weblite) - [Weblate](https://hosted.weblate.org/projects/guardianproject/keanu-weblite/)           |✅|✅|✅|✅|✅|✅| Served based on browser's preferred language.
|[likebutter.app](https://likebutter.app) - [Weblate](https://hosted.weblate.org/projects/guardianproject/butter-app-site/)           |✅|✅|✅|✅|✅|✅| Served based on browser's preferred language.

✅ = Supported.  Actual portion of strings translated at any time may vary; up to date stats available in weblate or from upstream project.

🟡 = Incompletely supported (e.g. on a per-app basis)

❌ = Not supported by this project

Note: We plan to support more languages in the future including Uighur
(`ug`), Russian (`ru`), and Ukrainian (`uk`) once pipelines in place for the
functions above.