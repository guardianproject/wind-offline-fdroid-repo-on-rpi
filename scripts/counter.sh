# Every minute or so, write a line to the log saying
# what time it is and how many users are in our RaspAP
# subnet.

echo "Monitoring uptime..."; while [ 1 ]; do nmap -sn "10.3.141.*" | grep "hosts up\|Starting" | tr '\n' ' '; echo ""; sleep 60; done >> /var/log/butter/client-count.log