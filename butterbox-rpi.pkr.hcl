packer {
  required_plugins {
    arm-image = {
      version = ">= 0.2.5"
      source  = "github.com/solo-io/arm-image"
    }
  }
}


source "arm-image" "butterbox-rpi" {
  iso_checksum      = "sha256:4fa99737265ac338a9ed0643f502246b97b928e5dfffa92939242e26e290638d"
  iso_url           = "https://downloads.raspberrypi.com/raspios_lite_armhf/images/raspios_lite_armhf-2024-03-15/2024-03-15-raspios-bookworm-armhf-lite.img.xz"
  target_image_size = "6442450944"
}

build {
  source "source.arm-image.butterbox-rpi" {
    name = "butterbox-rpi"
  }

  provisioner "shell" {
    inline = [       
      "systemctl enable ssh",
      "systemctl enable regenerate_ssh_host_keys",
      "sed -i 's/raspberrypi/butterbox/g' /etc/hosts",
      "echo 'butterbox' > /etc/hostname",
      "mkdir /tmp/butter-setup"
    ]

  }
  provisioner "file" {
    source = "./scripts/"
    destination = "/tmp/butter-setup/scripts"
  }

  provisioner "file" {
    source = "./configs/"
    destination = "/tmp/butter-setup/configs"
  }


  provisioner "shell" {
    scripts = [
      "./scripts/install-raspap.sh",
      "./scripts/install-ap-optimized-firmware.sh",
    "./scripts/remove-wifi-creds.sh", ]
    valid_exit_codes = [0, 1, 127]
    timeout          = "60m"

  }

  post-processor "compress" {
    output  = "{{.BuildName}}.xz"
    compression_level   = 9
    keep_input_artifact = false
  }

  post-processor "checksum" {
  checksum_types = ["sha1", "sha256"]
  output = "{{.BuildName}}_{{.ChecksumType}}.checksum"
  }


}