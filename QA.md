# QAing a newly built Butter Box

## Verifying your Butterbox is working properly

A functioning butterbox should be able to perform the following use cases.
These make good QA/Integration Tests for updates to the script, underlying
dependencies, or hardware.

Ideally, these tests are run with a factory reset android device.  If that's
not convenient, an android device without the butter app can also suffice.

### As a User, I'd like to install new apps (without any Internet connection to my device or the butterbox).

1. Disconnect the internet from the butterbox (if connected via Ethernet).
2. Turn on or power cycle the butterbox.
3. The butterbox should be broadcasting the SSID "butterbox".
4. Connect the device to the butterbox's wifi network, which should not
   require a password.
5. The captive portal should show up when the device connects or when the
   user attempts to load a website in the browser (behavior depends on the
   device).
6. Click "Continue" on the captive portal splash page.
7. The device should be redirected to "http://butterbox.lan" which should
   display a yellow page including a link to download the butter app.
	* Note: On some devices, the browser used to display captive portals may
	  not allow #8 or #9.  In this case, open http://butterbox.lan in an
	  ordinary browser.
8. The language should be toggleable between English and Spanish.
9. Clicking "Download Android App" should download the app.
10. The app package should be installable (but may require users to approve
    a new source, e.g. their browser).
11. When the Butter app is opened, a list of apps should be presented.
    Click an app not yet installed on the android device and install it.
12. The app should install (this may also require the user to give install
    permissions to Butter) and open without error.

### As a user, I'd like to install new apps (if my device sporadically has internet). 

1. After installing Butter per the use case above, connect to a wifi network
   that has Internet access.
2. Disable the last repository in the Butter Settings > Repositories menu.
   Ideally, this step would not be required, but F-Droid client seems to try
   to download from the first repository where it saw an app (in this case
   butterbox.lan) unless that repository is disabled.
3. Refresh the categories or latest menu in Butter.
4. Download and install an app not yet installed.

### As a user, I'd like to be able to use an ES-localized Butterbox 

0. Create a butterbox using the `-l es` flag & set the test phone's language
   to ES.
1. The butterbox should broadcast the network "comolamantequilla", which
   should not require a password.
2. The captive portal should appear in ES.
3. Clicking continuar should bring the user to http://comolamantequilla.lan,
   which should be rendered in ES.
4. The app should be downloadable and when downloaded, should appear on the
   homepage as "Mantequilla"
5. Application names and descriptions should appear in ES within Mantequilla
   and when installed.